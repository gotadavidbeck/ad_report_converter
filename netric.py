# importing pandas as pd
import pandas as pd
from datetime import datetime

rev_share = .2; #20% Rev share


df = pd.DataFrame(pd.read_excel("RP_Månadsavräkning-Netric_UTC-0800_202202010142.xlsx",0,
    skiprows = 0,
))

# Lookup-table Site-Domän
lookup_table = {
'Boras Tidning': 'bt.se',
'Kristianstadsbladet': 'kristianstadsbladet.se',
'Barometern OT': 'barometern.se',
'Ystads Allehanda': 'ystadsallehanda.se',
'Blekinge Lans Tidning': 'blt.se',
'Smalandsposten': 'smp.se',
'Norra Skåne': 'nsk.se',
'Sydostran': 'sydostran.se',
'Ulricehamns Tidning': 'ut.se', 
'Olandsbladet': 'olandsbladet.se',
'VXO News': 'vxonews.se',
'Trelleborgs Allehanda': 'trelleborgsallehanda.se',
'Kalmarposten.se': 'kalmarposten.se',
'Min Hockey': 'minhockey.se',
'Min Boll': 'minboll.se',
'BT Fotboll': 'btfotboll.se',
'Mera Näringsliv': 'meranaringsliv.se',
'VXO Week':'vxoweek.se',
'Mera Alvesta':'meraalvesta.se',
'Mera Ljungby':'meraljungby.se',
'Österlenmagasinet':'osterlenmagasinet.se',
'Mera Österlen':'meraosterlen.se',
'Växjöbladet Kronobergaren':'vaxjobladet.se',
'Boras DLY':'borasdly.se',
'Kalmar Läns Tidning':'klt.nu'
}

#Remove whitespace from Site

df["Site"] = df["Site"].str.strip()

#Replace Site with domain

df["Site"].replace(lookup_table, inplace=True)

#Concat and convert date

df['date'] = pd.to_datetime( df["Month"] + " " + df["Year"].astype(str) )

# Byt namn på Site
df = df.rename(columns={"Site":"site"})

#Byt namn på kolumn med intäkt
df = df.rename(columns={"Publisher Net Revenue":"earnings"})

#Byt namn på kolumn med impressions
df = df.rename(columns={"Paid Impressions":"impressions"})

# Ta bort onödiga kolumner
df = df[['site', 'date', 'earnings', 'impressions']]

# Lägg till saknade columner
df['channel_name'] = "Netric"
df['invoice_recipient'] = 159864
df['customer'] = 159864
df['channel_grouping'] = "Programmatic"
df['seller'] = "conmed01"

print(df.to_xml())