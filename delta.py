# importing pandas as pd
import pandas as pd
from datetime import datetime

df = pd.DataFrame(pd.read_excel("Delta_Gota_Media_2022_01.xlsx",0,
    skiprows = 0,
))

# Lookup-table Site-Domän
lookup_table = {
'Boras Tidning': 'bt.se',
'Borås Tidning': 'bt.se',
'Kristianstadsbladet': 'kristianstadsbladet.se',
'Barometern OT': 'barometern.se',
'Barometern': 'barometern.se',
'Ystads Allehanda': 'ystadsallehanda.se',
'Blekinge Lans Tidning': 'blt.se',
'Blekinge Läns Tidning': 'blt.se',
'Smalandsposten': 'smp.se',
'Smålandsposten': 'smp.se',
'Norra Skåne': 'nsk.se',
'Sydostran': 'sydostran.se',
'Sydöstran': 'sydostran.se',
'Ulricehamns Tidning': 'ut.se', 
'Olandsbladet': 'olandsbladet.se',
'Ölandsbladet': 'olandsbladet.se',
'VXO News': 'vxonews.se',
'Trelleborgs Allehanda': 'trelleborgsallehanda.se',
'Kalmarposten.se': 'kalmarposten.se',
'Kalmarposten': 'kalmarposten.se',
'Min Hockey': 'minhockey.se',
'Min Boll': 'minboll.se',
'BT Fotboll': 'btfotboll.se',
'Mera Näringsliv': 'meranaringsliv.se',
'VXO Week':'vxoweek.se',
'Mera Alvesta':'meraalvesta.se',
'Mera Ljungby':'meraljungby.se',
'Österlenmagasinet':'osterlenmagasinet.se',
'Mera Österlen':'meraosterlen.se',
'Växjöbladet Kronobergaren':'vaxjobladet.se',
'Boras DLY':'borasdly.se',
'Borås Daily':'borasdly.se',
'Mosaik VXO News':'mosaik.vxonews.se',
'Kalmar Läns Tidning':'klt.nu'
}

#Concat and convert date

# Byt namn på Date
df = df.rename(columns={"Date":"date"})

# Byt namn på Site
df = df.rename(columns={"Site":"site"})

#Byt namn på kolumn med intäkt
df = df.rename(columns={"Revenue":"earnings"})

#Byt namn på kolumn med impressions
df = df.rename(columns={"Sold Imp.":"impressions"})

# Ta bort onödiga kolumner
df = df[['site', 'date', 'earnings', 'impressions']]

df = df[df.date != "Total"] # remove Total

# Lägg till saknade columner
df['channel_name'] = "Delta Projects"
df['invoice_recipient'] = 175849
df['customer'] = 175179
df['channel_grouping'] = "Programmatic"
df['seller'] = "conmed01"

print(df.to_xml())