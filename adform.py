# importing pandas as pd
import pandas as pd

df = pd.DataFrame(pd.read_excel("Adform månadsavräkning_2022-02-01.xlsx",1,
    skiprows = 2,
))
df = df[df.Site != "TOTAL"] # remove Total
df = df.iloc[: , 1:]


df.columns = ['site', 'date', 'earnings']
df['channel_name'] = "Adform"
df['impressions'] = None
df['invoice_recipient'] = 168174
df['customer'] = 168174
df['channel_grouping'] = "Programmatic"
df['seller'] = "conmed01"


print(df.to_xml())