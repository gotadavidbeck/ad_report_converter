# importing pandas as pd
import pandas as pd

df = pd.DataFrame(pd.read_excel("concept.xlsx",1,
    skiprows = 0,
))

# Remove unnescessary columns

df = df.drop(columns=['CPM', 'Gross revenue'])

# Rename columns

df = df.rename(columns={'Placement' : 'site', 'Impressions' :'impressions', 'Net revenue Publisher 60%' : 'earnings'})

df['invoice_recipent'] = 175849
df['customer'] = 170445
df['channel_grouping'] = "Programmatic"
df['seller'] = "conmed01"

print(df.to_xml())