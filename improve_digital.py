# importing pandas as pd
import pandas as pd
from datetime import datetime

rev_share = .2; #20% Rev share


df = pd.DataFrame(pd.read_excel("report__4dbbc2c4-9372-42b6-ba82-5efa65cea0a8__2022-02-01-09-02-46.xlsx",0,
    skiprows = 0,
))

# Lookup-table Site-Domän
lookup_table = {
'Boras Tidning': 'bt.se',
'Borås Tidning': 'bt.se',
'Kristianstadsbladet': 'kristianstadsbladet.se',
'Barometern OT': 'barometern.se',
'Barometern': 'barometern.se',
'Ystads Allehanda': 'ystadsallehanda.se',
'Blekinge Lans Tidning': 'blt.se',
'Blekinge Läns Tidning': 'blt.se',
'Smalandsposten': 'smp.se',
'Smålandsposten': 'smp.se',
'Norra Skåne': 'nsk.se',
'Sydostran': 'sydostran.se',
'Sydöstran': 'sydostran.se',
'Ulricehamns Tidning': 'ut.se', 
'Olandsbladet': 'olandsbladet.se',
'Ölandsbladet': 'olandsbladet.se',
'VXO News': 'vxonews.se',
'Trelleborgs Allehanda': 'trelleborgsallehanda.se',
'Kalmarposten.se': 'kalmarposten.se',
'Kalmarposten': 'kalmarposten.se',
'Min Hockey': 'minhockey.se',
'Min Boll': 'minboll.se',
'BT Fotboll': 'btfotboll.se',
'Mera Näringsliv': 'meranaringsliv.se',
'VXO Week':'vxoweek.se',
'Mera Alvesta':'meraalvesta.se',
'Mera Ljungby':'meraljungby.se',
'Österlenmagasinet':'osterlenmagasinet.se',
'Mera Österlen':'meraosterlen.se',
'Växjöbladet Kronobergaren':'vaxjobladet.se',
'Boras DLY':'borasdly.se',
'Borås Daily':'borasdly.se',
'Mosaik VXO News':'mosaik.vxonews.se',
'Kalmar Läns Tidning':'klt.nu'
}

#Remove whitespace from Site

df["Site Name"] = df["Site Name"].str.strip()

#Replace Site with domain

df["Site Name"].replace(lookup_table, inplace=True)

#Concat and convert date

df['date'] = pd.to_datetime( df["Month"] )

# Byt namn på Site
df = df.rename(columns={"Site Name":"site"})

#Byt namn på kolumn med intäkt
df = df.rename(columns={"Revenue (SEK)":"earnings"})

#Byt namn på kolumn med impressions
df = df.rename(columns={"Won Bids":"impressions"})

# Ta bort onödiga kolumner
df = df[['site', 'date', 'earnings', 'impressions']]

# Lägg till saknade columner
df['channel_name'] = "Improve Digital"
df['invoice_recipient'] = 175149
df['customer'] = 177562
df['channel_grouping'] = "Programmatic"
df['seller'] = "conmed01"

print(df.to_xml())