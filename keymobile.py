# importing pandas as pd
import pandas as pd
import numpy as np

filename = "Revshare_Keymobile_GotaMedia_2022_01.xlsx"

df = pd.DataFrame(pd.read_excel(filename,1,
    skiprows = 0,
))

# Load specific cell for date

# Read Excel and select a single cell (and make it a header for a column)
df_date = pd.read_excel(filename, 0, index_col=None, usecols = "C", header = 4, nrows=0)

# Subtract one month

d = df_date.columns.values[0] - np.timedelta64(1,'m')

df['date'] = d  

# Remove unnescessary columns

df = df.drop(columns=['CPM', 'Gross revenue'])

# Rename columns

df = df.rename(columns={'Placement' : 'site', 'Impressions' :'impressions', 'Net revenue Publisher 60%' : 'earnings'})

# Remove "-DESKTOP" from Placement

df['site'].replace(to_replace=r'\ - DESKTOP', value='', regex=True , inplace=True)

df = df.groupby( [ "site" ] ).sum().reset_index()

df['channel_name'] = "Keymobile"
df['invoice_recipient'] = 175849
df['customer'] = 170445
df['channel_grouping'] = "Programmatic"
df['seller'] = "conmed01"

print(df.to_xml())

#lägg till channel_name = "Adform"
#filnamn = channel_[filestamp].xml

