# importing pandas as pd
import pandas as pd

df = pd.DataFrame(pd.read_excel("SUNT Publisher Invoicing - Gota-traffic.xlsx",0,
    skiprows = 6,
))

# Load specific cell for date

# Read Excel and select a single cell (and make it a header for a column)
df_date = pd.read_excel('SUNT Publisher Invoicing - Gota-traffic.xlsx', 0, index_col=None, usecols = "F", header = 3, nrows=0)

excel_date = df_date.columns.values[0]

# Remove total

df = df[df.Site != "Total"] # remove Total

# Rename columns

df = df.rename(columns={'Site' : 'site', 'Ad imps' :'impressions', 'Revenue (net)' : 'earnings'})

# Ta bort onödiga kolumner
df = df[['site', 'earnings', 'impressions']]


df['date'] = excel_date
df['channel_name'] = "SUNT"
df['invoice_recipient'] = 158124
df['customer'] = 158124
df['channel_grouping'] = "Native"
df['seller'] = "conmed01"

print(df.to_xml())

#lägg till channel_name = "Adform"
#filnamn = channel_[filestamp].xml

