# importing pandas as pd
import pandas as pd

def to_xml(df):

    xml = ['<?xml version="1.0" encoding="UTF-8"?><items>']

    def row_xml(row):
        xml.append('<item>')
        for i, col_name in enumerate(row.index):
            xml.append('  <{0}>{1}</{0}>'.format(''.join(e for e in col_name if e.isalnum()), row.iloc[i]))
        xml.append('</item>')
        return '\n'.join(xml)
    
    res = '\n'.join(df.apply(row_xml, axis=1))
    xml.append(res)
    xml.append("</items>")

    return(''.join(xml))
      
# read an excel file and convert 
# into a dataframe object

# df = pd.DataFrame(pd.read_excel("gs://gota-media-file-conversion/Pubmatic - PUBMATIC - april 2021.xlsx",2))

rev_share = .2; #20% Rev share

df = pd.DataFrame(pd.read_excel("160016-Historical Report-20210501T00_00-20210531T23_59 (1).xls",0,
    skiprows = 1,
))

df['Site'] = df['Site'].str.split('/').str[0]


# sum_df = df.groupby(['Site'])['Revenue(Kr)'].sum().reset_index()

sum_df = df.groupby(['Site']).agg({'Revenue(Kr)': "sum"}).reset_index()


# sum_df = df.groupby( ['Site'] ).agg('sum')*(1-rev_share)

sum_df['Revenue(Kr)'] = sum_df['Revenue(Kr)']*(1-rev_share)

sum_df = sum_df.rename(columns={"Revenue(Kr)":"Revenue"})

print(  sum_df.to_xml()  )


#print( sum_df[['Site','Revenue(Kr)']] )

# show the dataframe
# print( to_xml( df.groupby(['Domain']).sum() ) )

