# importing pandas as pd
import pandas as pd

df = pd.DataFrame(pd.read_excel("Strossle - Gota januari 2022.xlsx",0,
    skiprows = 0,
))

# Remove all summarizing rows. If Publisher contains Gota Media, lets keep it

df = df[df.Publisher == "Gota Media AB"]

# Split after _

df['site'] = df['Widget'].str.split('_').str[0]

# Replace for apps

# Lookup-table Site-Domän
lookup_table = {
'Boras Tidning': 'bt.se',
'kristianstadsbladet': 'kristianstadsbladet.se',
'Barometern': 'barometern.se',
'Oskarshamns-Tidningen': 'barometern.se',
'ystadsallehanda': 'ystadsallehanda.se',
'blt': 'blt.se',
'smp': 'smp.se',
'Norra Skåne': 'nsk.se',
'sydostran': 'sydostran.se',
'ut': 'ut.se', 
'Olandsbladet': 'olandsbladet.se',
'VXO News': 'vxonews.se',
'trelleborgsallehanda': 'trelleborgsallehanda.se',
'kalmarposten': 'kalmarposten.se',
'Min Hockey': 'minhockey.se',
'Min Boll': 'minboll.se',
'BT Fotboll': 'btfotboll.se',
'Mera Näringsliv': 'meranaringsliv.se',
'VXO Week':'vxoweek.se',
'Mera Alvesta':'meraalvesta.se',
'Mera Ljungby':'meraljungby.se',
'Österlenmagasinet':'osterlenmagasinet.se',
'Mera Österlen':'meraosterlen.se',
'Växjöbladet Kronobergaren':'vaxjobladet.se',
'Boras DLY':'borasdly.se',
'Kalmar Läns Tidning':'klt.nu'
}

#Replace Site with domain

df["site"].replace(lookup_table, inplace=True)

# Rename columns

df = df.rename(columns={'ad_imps' :'impressions', 'pub rev' : 'earnings'})

#Convert date

df['date'] = pd.to_datetime( df["Month"] )

# Ta bort onödiga kolumner
df = df[['date', 'site', 'earnings', 'impressions']]

#Summera

df = df.groupby( ["site", pd.Grouper(key="date", freq='M') ] ).sum().reset_index()

df['channel_name'] = "Strossle"
df['invoice_recipient'] = 175849
df['customer'] = 176457
df['channel_grouping'] = "Native"
df['seller'] = "conmed01"

print(df.to_xml())

#lägg till channel_name = "Adform"
#filnamn = channel_[filestamp].xml