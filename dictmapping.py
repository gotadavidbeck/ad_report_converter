import fnmatch

def converter_pubmatic(file):
    return "pubmatic_" + file

def converter_rubicon(file):
    return "rubicon"

def no_converter_found(file):
    return "Not found"

def match_file_to_converter(file):

    # Match function to pattern in file  
    
    if fnmatch.fnmatch(file, 'Pubmatic - PUBMATIC -*.xlsx'):
        print("Pubmatic")
    elif fnmatch.fnmatch(file, 'Rubicon*.xlsx'):
        print("Rubicon")
    else :
        print ("No matching converter found.")

print ( 
    match_file_to_converter("Pubmatic - PUBMATIC - april 2021.xlsx") 
    )

print ( 
    match_file_to_converter("Rubicon - PUBMATIC - april 2021.xlsx") 
    )    

print ( 
    match_file_to_converter("Rumpadumper - PUBMATIC - april 2021.xlsx") 
    )    