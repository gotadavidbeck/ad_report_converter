# importing pandas as pd
import pandas as pd
from datetime import datetime

rev_share = .2; #20% Rev share


df = pd.DataFrame(pd.read_excel("160016-Historical Report-20220101T00_00-20220131T23_59.xls",0,
    skiprows = 1,
))


# Hitta toppen av site

df['site'] = df['Site'].str.split('/').str[0]

# Ändra format på Datum till timestamp

df['Date'] = pd.to_datetime(df['Date'])

# Gruppera på månad och sajt

df = df.groupby( ["site", pd.Grouper(key="Date", freq='M') ] ).sum().reset_index()

# Räkna bort rev share
df['Revenue(Kr)'] *= (1-rev_share)

#Byt namn på kolumn med intäkt
df = df.rename(columns={"Revenue(Kr)":"earnings"})

#Byt namn på kolumn med impressions
df = df.rename(columns={"Paid Impressions":"impressions"})

#Byt namn på kolumn med datum
df = df.rename(columns={"Date":"date"})

# Ta bort onödiga kolumner
df = df[['site', 'date', 'earnings', 'impressions']]

# Lägg till saknade columner
df['channel_name'] = "Pubmatic"
df['invoice_recipient'] = 146825
df['customer'] = 146825
df['channel_grouping'] = "Programmatic"
df['seller'] = "conmed01"

print(df.to_xml())